<?php
require 'endavo.php';

$code = (isset($_POST["id"]) || isset($_GET["id"])) ? (isset($_POST["id"]) ? $_POST["id"] : $_GET["id"]) : "0";

if($code == 0){
  die("No se envió un código");
}

$endavo = new EmmsApiClientTest();

$result = $endavo->test($code);

if($result["status"] == "ok"){

  $item = json_encode($result["result"]);
  $item = json_decode($item, true);
  $adaptive = json_encode($item["adaptive"][0]);
  $adaptive = json_decode($adaptive, true);
  $adaptive = $adaptive["url"];

  $tm_video_code = "<div id=\"jwppp-video-box-73641\" style=\"margin: 1rem 0;\">\n<div id=\"jwppp-video-73641\" class=\"jwplayer jw-reset jw-state-idle jw-skin-seven jw-stretch-uniform jw-breakpoint-5 jw-flag-user-inactive\" tabindex=\"0\"></div>\n</div>\n<script type=\"text/javascript\">\nvar playerInstance_73641 = jwplayer(\"jwppp-video-73641\");\nplayerInstance_73641.setup({\nfile: \"".$adaptive."\",\nadvertising: {client: \"vast\", \n\"skipoffset\":10,\ntag:\"https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/32830837/Ranchito-Play_PreRoll&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&url=[referrer_url]&description_url=[description_url]&correlator=[timestamp]\"},\nwidth: \"100%\",\naspectratio: \"16:9\",\nga: {},\n})\n</script>";

  $arr = array("status" => "ok", "code" => $tm_video_code);

  echo json_encode($arr, JSON_PRETTY_PRINT);

} else {
  echo "Error al obtener la información del video";
}
?>
