# Ranchito Play Auto Updater #

Actualizaciones automáticas por parte de un RSS en Endavo para uso de Ranchito Play

### ¿Para qué sirve? ###

* Actualizar automáticamente el contenido de Ranchito Play por medio de un RSS y API de Endavo

### ¿Cómo configurarlo? ###

* Descargar los archivos al servidor
* Cambiar el Client Key y Secret Key en el archivo endavo.php
* Cambiar el nombre de usuario y contraseña de la base de datos en BDConnection.php
* Si hay cambios en las categorías, agregarlas o modificarlas en el archivo index.php

### Propiedad ###

* Telemetrika S. A. de C. V.
* Ing. Ricardo Santoyo Reyes, Telemetrika
