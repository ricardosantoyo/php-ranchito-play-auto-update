<?php
require 'endavo.php';
require 'BDConnection.php';

$ENDAVO = new RanchitoPlayAutoUpdate();

$array = $ENDAVO->getLatest();
$array_compared = $ENDAVO->compareLatest($array);
if($array_compared["status"] == "ok"){
  $ENDAVO->insertLatest($array_compared);
}

//Welcome to the Auto Update code for Ranchito Play!!
//Please take a seat and remember to keep your arms and legs in your desk all the time.
//Hope you understand what I did, I think I made it simple but that's because I know what I did.
//If you want help or dont understand something... may god have mercy on you.

//Remember to always output text in Spanish.
class RanchitoPlayAutoUpdate{
  public $latest = NULL;
  public $json_endavo;

  public function __construct() {

	}

  function getLatest(){
    //We connect to the RSS file to get the latest 15 videos.
    $url = "http://ultratelecom.endavomedia.com/rss/custom/7559/";
    $latest = file_get_contents($url);
    $x = new SimpleXmlElement($latest);

    //And we turn them to a really pretty JSON
    $json = json_encode($x, JSON_UNESCAPED_UNICODE);

    $json_endavo = array(
      "status" => "ok",
      "desc" => "Full list of rss entries"
    );

    try{
      //We get the object form the JSON to use it a little easier.
      $json_obj = json_decode($json, true);
      foreach($json_obj["channel"]["item"] as $e){
        $exp_id = explode("-", $e["guid"]);
        $id = $exp_id[0];

        //Okay, now we connect to the endavo API in order to get the data from the RSS items
        $endavo_api = new EmmsApiClientTest();

        //This is where the call is being made, the method is called 'test', I'm too lazy to change its name...
        $result = $endavo_api->test($id);

        if($result == NULL){
          return array("status" => "err", "desc" => "Hubo un error al obtener los datos de los videos");
        } else if($result["status"] == "ok"){

          //We save the endavo items to an array
          $json_endavo["results"][] = $result["result"];

        }

      }

      return $json_endavo;

    } catch(Exception $e){
      $err = array("status" => "err", "desc" => "Ocurrió un error al intentar obtener las últimas entradas en el RSS.");

      $this->saveLog($err);

      return $err;
    }
  }

  function compareLatest($array){
    //echo "<pre>" . json_encode($array, JSON_UNESCAPED_UNICODE) . "</pre>";

    $_BD= new connection();

    $conn = $_BD->connect();

    if($conn == NULL){
      //Couldn't connect to the database, show error
      $result = array(
        "status" => "err",
        "desc" => "Error al intentar conectarse a la base de datos"
      );

      $this->saveLog($result);

      return $result;
    }

    $result = array(
      "status" => "ok",
      "desc" => "Lista de entradas ya comparadas"
    );

    foreach($array["results"] as $item){
      //ONLY current year videos
      //That's because this code was made when we already had a SHIT-TON of videos on the site from last year (2016)
      //and none of them had the '_endavo_id' on their meta data
      $year = date("Y");
      if(strpos($item["orderDateTime"], $year) !== false){

        try{
          //Okay, we must compare if the videos from the RSS are already inserted.
          //If '_endavo_id' isn't in the DB, then it adds the item to an array.
          $query = "SELECT * FROM wp_ranchxtspostmeta WHERE meta_key='_endavo_id' AND meta_value=".$item["id"].";";

          $res = $conn->prepare($query);
          $res->execute();

          //If there are any result sets, then save them
          if($res->rowCount() == 0){
            $result["results"][] = $item;
          }

        } catch (PDOException $e){
          $result = array(
            "status" => "err",
            "desc" => $e->getMessage()
          );

          return $result;
        }
      }
    }

    return $result;
  }

  function insertLatest($array){
    $_BD= new connection();

    $conn = $_BD->connect();

    $result = array("status" => "ok");

    try{

      //We must compare if there are entries or nah.
      //Why would you want to insert a video if there are not videos to insert?
      if(!array_key_exists("results", $array)){
        $timezone  = -6;
        $date = gmdate("Y-m-d H:i:s", time() + 3600*($timezone+date("I")));
        $result["date"] = $date;
        $result["desc"] = "No hay nuevas entradas. Ejecución terminada.";

        $this->saveLog($result);

        echo "<pre>" . json_encode($result, JSON_UNESCAPED_UNICODE) . "</pre>";

        return;
      }

      //We insert each item on the DB, this is where the magic happens
      $item_index = 0;
      foreach($array["results"] as $item){

        //IMPORTANT: Check if the item isn't an audio file
        if($item["type"] == "audio"){
          $item_index++;
          $timezone  = -6;
          $date = gmdate("Y-m-d H:i:s", time() + 3600*($timezone+date("I")));
          $result["status"] = "err";
          $result["date"] = $date;
          $result["desc"] = "Archivo de audio encontrado, se omitirá. Endavo ID: ".$item["id"];

          $this->saveLog($result);

          echo "<pre>" . json_encode($result, JSON_UNESCAPED_UNICODE) . "</pre>";

          if($item_index == count($array["results"])) {
            $result = array();
            $result["status"] = "ok";
            $result["date"] = $date;
            $result["desc"] = "No hay nuevas entradas. Ejecución terminada.";

            $this->saveLog($result);

            echo "<pre>" . json_encode($result, JSON_UNESCAPED_UNICODE) . "</pre>";

            return;
          }

          continue;
        }

        $slug = $this->friendly_url($item["slug"]);

        $non_chars = array('\'', '"', '\\\'', '\\"', '\\');
        $item_title = str_replace($non_chars, "", $item["title"]);
        $item_description = str_replace($non_chars, "", $item["description"]);

        $query = 'INSERT INTO wp_ranchxtsposts '.
            '(post_author, post_date, post_date_gmt, post_content, post_title, post_excerpt, post_status, comment_status, ping_status, post_name, to_ping, pinged, post_modified, post_modified_gmt, post_content_filtered, post_parent, guid, menu_order, post_type, comment_count) '.
            'VALUES '.
            '(8,'.//post_author: Author, 8 means Cristian
              '"'.$item["orderDateTime"].'",'.//post_date: Date of publication
              '"'.$item["orderDateTime"].'",'.//post_date_gmt: GMT date of publication
              '\'<span data-sheets-value="{"1":2, "2":"'.addslashes( $item_description ).'"}" data-sheets-userformat="{"2":513, "3":{"1":0},"12":0}">'.addslashes( $item_description ).'</span>\','.//post_content: Description
              '"'.addslashes( $item_title ).'",'.//post_title: Title of the video
              '"",'.//post_excerpt: Excerpt, we don't handle them
              '"publish",'.//post_status: Status to publish it
              '"open",'.//comment_status: Can comment on video
              '"open",'.//ping_status: Can access it by ping
              '"'.$slug.'",'.//post_name: Slug for the video entry
              '"",'.//to_ping: Not important
              '"",'.//pinged: Not important
              '"'.$item["orderDateTime"].'",'.//post_modified: Date of the last modification
              '"'.$item["orderDateTime"].'",'.//post_modified_gmt: Date of the last modification GMT
              '"",'.//post_content_filtered: Not important
              '0,'.//post_parent: Parent of the post, mostly 0
              '"",'.//guid: URL access to the post, will be updated next with the post ID
              '0,'.//menu_order: Always 0
              '"post",'.//post_type: Could be 'attachment' for images, but let's keep it as 'post'
              '0'.//comment_count: How many comments the post has? always 0 because nobody ever comments. (just kidding, please don't fire me)
            ');';

        $res = $conn->prepare($query);
        $res->execute();

        //We update the guid of the entry with the ID from the insertion
        $id = $conn->lastInsertId();
        $query = "UPDATE wp_ranchxtsposts SET guid='http://ranchitoplay.com/?p=".$id."' WHERE ID=".$id.";";
        $conn->exec($query);

        //We copy the image to the server
        $imageRes["status"] = "err/image";
        if(array_key_exists("images", $item)){
          $imageRes = $this->copyImage($item["images"]);
        }

        //Then we insert the meta data for each post
        $timezone  = -6;
        $date = gmdate("Y-m-d H:i:s", time() + 3600*($timezone+date("I")));
        $result["date"] = $date;

        //And we insert the image data to a new post
        if($imageRes["status"] == "ok"){
          $image_flag = $this->insertPostImage($imageRes, $id);
        }

        if($this->insertPostmeta($conn, $id, $item)){
          $result["results"][] = array("item-id" => $id, "item-endavo-id" => $item["id"], "postmeta" => "success", "image" => $image_flag);
        } else {
          $result["results"][] = array("item-id" => $id, "item-endavo-id" => $item["id"], "postmeta" => "error", "image" => $image_flag);
        }

      }

      $this->saveLog($result);

      echo "<pre>" . json_encode($result, JSON_UNESCAPED_UNICODE) . "</pre>";

    } catch (PDOException $e){
      $timezone  = -6;
      $date = gmdate("Y-m-d H:i:s", time() + 3600*($timezone+date("I")));

      $err = array(
        "status" => "err",
        "date" => $date,
        "desc" => $e->getMessage()
      );

      $this->saveLog($err);

      echo "<pre>" . json_encode($err, JSON_UNESCAPED_UNICODE) . "</pre>";
      return;
    }
  }

  /**
  * Function to insert image's post and postmeta data to the DB
  **/
  function insertPostImage($imageRes, $id){
    $aux_img = json_encode($imageRes);
    $imageRes = json_decode($aux_img, true);

    $_BD= new connection();

    $conn = $_BD->connect();

    $image = $imageRes["results"];

    //In order to display the image we need to know which is its metatype and save it respectively.
    if($image["extension"] == "jpg"){
      $mime_type = "jpeg";
    } else {
      $mime_type = $image["extension"];
    }

    try{

      $timezone  = -6;
      $date = gmdate("Y-m-d H:i:s", time() + 3600*($timezone+date("I")));

      //This is where we insert the image data as a post, later we have to insert its meta data
      $query = 'INSERT INTO wp_ranchxtsposts '.
          '(post_author, post_date, post_date_gmt, post_content, post_title, post_excerpt, post_status, comment_status, ping_status, post_name, to_ping, pinged, post_modified, post_modified_gmt, post_content_filtered, post_parent, guid, menu_order, post_type, post_mime_type, comment_count) '.
          'VALUES '.
          '(1,'.//post_author: Author, 1 means Ricardo
            '"'.$date.'",'.//post_date: Date of publication
            '"'.$date.'",'.//post_date_gmt: GMT date of publication
            '"",'.//post_content: Description, not important here
            '"'.$image["name"].'",'.//post_title: Title of the video
            '"",'.//post_excerpt: Excerpt, we don't handle them
            '"inherit",'.//post_status: Status to publish it
            '"open",'.//comment_status: Can comment on video
            '"closed",'.//ping_status: Can access it by ping
            '"'.$image["name"].'",'.//post_name: Slug for the video entry
            '"",'.//to_ping: Not important
            '"",'.//pinged: Not important
            '"'.$date.'",'.//post_modified: Date of the last modification
            '"'.$date.'",'.//post_modified_gmt: Date of the last modification GMT
            '"",'.//post_content_filtered: Not important
            '0,'.//post_parent: Parent of the post, mostly 0
            '"http://ranchitoplay.com/wp-contents/uploads/endavo/'.$image["filename"].'",'.//guid: URL access to the image
            '0,'.//menu_order: Always 0
            '"attachment",'.//post_type: Could be 'attachment' for images, but let's keep it as 'post'
            '"image/'.$mime_type.'",'.//post_mime_type: image/+extension
            '0'.//comment_count: How many comments the post has? always 0 because nobody ever comments. (just kidding, please don't fire me)
          ');';
          //Did you notice I just copy-pasted the query from the one on the post? LOL

      $res = $conn->prepare($query);
      $res->execute();
      $imageId = $conn->lastInsertId();

      //The meta data of the image. I'm not quite sure what this means, but if you don't know what you are doing, leave it intact.
      $att_meta = 'a:5:{s:5:"width";i:500;s:6:"height";i:500;s:4:"file";s:24:"endavo/'.$image["filename"].'";s:5:"sizes";a:13:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"endavo/'.$image["filename"].'";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"endavo/'.$image["filename"].'";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"videopro_misc_thumb_1";a:4:{s:4:"file";s:22:"endavo/'.$image["filename"].'";s:5:"width";i:50;s:6:"height";i:50;s:9:"mime-type";s:10:"image/jpeg";}s:21:"videopro_misc_thumb_2";a:4:{s:4:"file";s:23:"endavo/'.$image["filename"].'";s:5:"width";i:100;s:6:"height";i:75;s:9:"mime-type";s:10:"image/jpeg";}s:21:"videopro_misc_thumb_3";a:4:{s:4:"file";s:24:"endavo/'.$image["filename"].'";s:5:"width";i:205;s:6:"height";i:115;s:9:"mime-type";s:10:"image/jpeg";}s:21:"videopro_misc_thumb_4";a:4:{s:4:"file";s:24:"endavo/'.$image["filename"].'";s:5:"width";i:277;s:6:"height";i:156;s:9:"mime-type";s:10:"image/jpeg";}s:21:"videopro_misc_thumb_5";a:4:{s:4:"file";s:24:"endavo/'.$image["filename"].
        '";s:5:"width";i:298;s:6:"height";i:298;s:9:"mime-type";s:10:"image/jpeg";}s:21:"videopro_misc_thumb_6";a:4:{s:4:"file";s:24:"endavo/'.$image["filename"].'";s:5:"width";i:320;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:21:"videopro_misc_thumb_7";a:4:{s:4:"file";s:24:"endavo/'.$image["filename"].'";s:5:"width";i:407;s:6:"height";i:229;s:9:"mime-type";s:10:"image/jpeg";}s:21:"videopro_misc_thumb_8";a:4:{s:4:"file";s:24:"endavo/'.$image["filename"].'";s:5:"width";i:500;s:6:"height";i:318;s:9:"mime-type";s:10:"image/jpeg";}s:21:"videopro_misc_thumb_9";a:4:{s:4:"file";s:24:"endavo/'.$image["filename"].'";s:5:"width";i:500;s:6:"height";i:358;s:9:"mime-type";s:10:"image/jpeg";}s:22:"videopro_misc_thumb_10";a:4:{s:4:"file";s:24:"endavo/'.$image["filename"].'";s:5:"width";i:500;s:6:"height";i:450;s:9:"mime-type";s:10:"image/jpeg";}s:14:"tptn_thumbnail";a:4:{s:4:"file";s:24:"endavo/'.$image["filename"].
          '";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}';

      //We insert the meta data with the ID of the image post, and the ID of the original post with the thumbnail ID.
      $query = 'INSERT INTO wp_ranchxtspostmeta (post_id, meta_key, meta_value) VALUES '.
          '("'.$imageId.'", "_wp_attached_file", "endavo/'.$image["filename"].'"), '.
          '("'.$imageId.'", "_wp_attachment_metadata", \''.$att_meta.'\'), '.
          '("'.$id.'", "_thumbnail_id", "'.$imageId.'");';


      $res = $conn->prepare($query);
      $res->execute();

      return true;
    } catch (PDOException $e){
        $timezone  = -6;
        $date = gmdate("Y-m-d H:i:s", time() + 3600*($timezone+date("I")));

        $err = array(
          "status" => "err/image",
          "date" => $date,
          "desc" => $e->getMessage()
        );

        $this->saveLog($err);

        echo "<pre>" . json_encode($err, JSON_UNESCAPED_UNICODE) . "</pre>";
        return false;
    }
  }


  function insertPostmeta($conn, $id, $item){
    try{
      //IMPORTANT: The script for the video to show.
      //IMPORTANTER: Don't change it unless you know what you are doing.
      $adaptive = json_encode($item["adaptive"][0]);
      $adaptive = json_decode($adaptive, true);
      $adaptive = $adaptive["url"];

      $renditions = json_encode($item["renditions"][count($item["renditions"] )- 1]);
      $renditions = json_decode($renditions, true);
      $renditions = $renditions["hlsUrl"];

      $video_file = $adaptive == "" ? $renditions : $adaptive;

      $tm_video_code = "<div id=\"jwppp-video-box-73641\" style=\"margin: 1rem 0;\">\n<div id=\"jwppp-video-73641\" class=\"jwplayer jw-reset jw-state-idle jw-skin-seven jw-stretch-uniform jw-breakpoint-5 jw-flag-user-inactive\" tabindex=\"0\"></div>\n</div>\n<script type=\"text/javascript\">\nvar playerInstance_73641 = jwplayer(\"jwppp-video-73641\");\nplayerInstance_73641.setup({\nfile: \"".$video_file."\",\ncast: {},\nadvertising: {\nclient: \"vast\", \n\"skipoffset\":10,\ntag:\"https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/32830837/Ranchito-Play_PreRoll&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&url=[referrer_url]&description_url=[description_url]&correlator=[timestamp]\"\n},\nwidth: \"100%\",\naspectratio: \"16:9\",\nsharing: {\nsites: [\"facebook\",\"twitter\",\"tumblr\"]\n},\nga: {},\n})\n</script>";
      //$tm_video_code = "<script type=\"text/javascript\">\nvar _embedPlayerParams = _embedPlayerParams || [];\n_embedPlayerParams[\"_mediaHash\"] = \"".$item["hash"]."\";\n_embedPlayerParams[\"_clientId\"] = \"bpktatpfXvv2wjV3MBew3lncNkMW6OpR\";\n_embedPlayerParams[\"_width\"] = \"auto\";\n_embedPlayerParams[\"_height\"] = \"auto\";\n_embedPlayerParams[\"_autoPlay\"] = 0;\n</script>\n<script src=\"//player.endavomedia.com/show.js\" type=\"text/javascript\"></script>";
      //$tm_video_file = $item["video"];

      $query = 'INSERT INTO wp_ranchxtspostmeta (post_id, meta_key, meta_value) VALUES '.
        '('.$id.', "_edit_lock", "1482860667:8"),'.//Couldn't decipher what this number means, but it's always this
        '('.$id.', "_vc_post_settings", "a:1:{s:10:\"vc_grid_id\";a:0:{}}"),'.//Settings for the template
        '('.$id.', "edit_last", "8"),'.//Who edited it last time, 8 means Cristian
        '('.$id.', "_hide_featured", NULL),'.//If we want the video to not appear on featured entries, then we just change it to 'on'
        '('.$id.', "order_series", "0"),'.//We don't handle series, so it's always gonna be 0
        '('.$id.', "tm_video_code", '.$conn->quote($tm_video_code).'),'.//IMPORANT: the script for the video to show
        '('.$id.', "user_rate_option", "on"),'.//If users can rate it or nah
        '('.$id.', "post_sidebar", "0"),'.//We don't want a sidebar on our videos
        '('.$id.', "enable_live_video", "off"),'.//Are we streaming or nah?... Nah bruh
        '('.$id.', "tm_multi_link", NULL),'.//We just handle a single video per entry, why would you want to turn this on?... But if you want, then change it to 'on'
        '('.$id.', "_endavo_id", "'.$item["id"].'")';//Custom BD entry for the system to know if it should insert a video. As the name says, it has the Endavo ID of the entry

      $res = $conn->prepare($query);
      $res->execute();

      //Okay, we reached the final part of the meta insertion, you should be happy.
      //We must compare categories because the channels of endavo and the wordpress categories are different
      //so we have to know which one is which.

      $json_categories = file_get_contents("categories.json");

      $JSON_ITERATOR = new RecursiveIteratorIterator(
        new RecursiveArrayIterator(json_decode($json_categories, true)),
        RecursiveIteratorIterator::SELF_FIRST
      );

      $aux = json_encode($item["channels"][0]);
      $category_obj = json_decode($aux, true);

      $category = 1;

      foreach ($JSON_ITERATOR as $key => $val) {
        if ($key === $category_obj["name"]){
          $category = $val["category"];
        }
      }

      $query = "INSERT INTO wp_ranchxtsterm_relationships (object_id, term_taxonomy_id, term_order) ".
               "VALUES (" . $id . ", " . $category . ", 0),(" . $id . ", 168, 0)";

      $res = $conn->prepare($query);
      $res->execute();

      return true;
    } catch (PDOException $e){
      $err = array(
        "status" => "err",
        "desc" => "Error insertar postmeta/category: " . $e->getMessage()
      );

      $this->saveLog($err);

      echo "<pre>" . json_encode($err, JSON_UNESCAPED_UNICODE) . "</pre>";
      return false;
    }
  }

  //Here's where we copy the image to the page server, this is because wordpress won't recognize
  //images from an external server.
  function copyImage($array){
    $aux = 0;
    $url = "";
    foreach($array as $image){
      $aux_img = json_encode($image);
      $item = json_decode($aux_img, true);

      //We choose the image with best resolution
      if(intval($item["width"]) > $aux){
        $aux = intval($item["width"]);
        $url = $item["url"];
      }
    }

    if($url == "") { return; };

    //With pathinfo we can get the file's meta data, but we are interested in the extension
    $info = pathinfo($url);

    //With basename we get the filename without the extension
    $filename = basename($url, ".".$info["extension"]);

    //The path where we are going to copy the new file
    $path = "../wp-content/uploads/endavo/".$filename.".".$info["extension"];
    if(!@copy($url, $path)){
      //If there's an error, we save the log
      $timezone  = -6;
      $date = gmdate("Y-m-d H:i:s", time() + 3600*($timezone+date("I")));

      $err = array(
        "status" => "err/image",
        "date" => $date,
        "results" => array(
          "desc" => "No se ha podido copiar la imagen al servidor.",
          "url" => $url,
          "path" => $path,
          "error" => error_get_last()
        ),
        "imageUrl" => $url
      );

      $this->saveLog($err);

      return $err;
    } else {
      //We return the array with the data of the image to insert it in the DB
      $res = array(
        "status" => "ok",
        "results" => array(
          "url" => $url,
          "filename" => $filename.".".$info["extension"],
          "name" => $filename,
          "extension" => $info["extension"]
        ),
        "desc" => "Imagen copiada satisfactoriamente"
      );

      return $res;
    }
  }

  function saveLog($array){
    $timezone  = -6;
    $date = gmdate("Y-m-d H:i:s", time() + 3600*($timezone+date("I")));

    $_BD= new connection();

    $conn = $_BD->connect();

    if(array_key_exists("results", $array)){
      $content = $array["results"];
    } else {
      $content = $array["desc"];
    }

    $non_chars = array('\'', '"', '\\\'', '\\"');
    $new_content = str_replace($non_chars, "", json_encode($content, JSON_UNESCAPED_UNICODE));

    $query = "INSERT INTO wp_ranchxtsendavo_log (log_date, status, content) VALUES ('".$date."', '".$array["status"]."', '".$new_content."');";

    $res = $conn->prepare($query);
    $res->execute();
  }

  function friendly_url($string){
      $string = str_replace(array('[\', \']'), '', $string);
      $string = preg_replace('/\[.*\]/U', '', $string);
      $string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '-', $string);
      $string = htmlentities($string, ENT_COMPAT, 'utf-8');
      $string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string );
      $string = preg_replace(array('/[^a-z0-9]/i', '/[-]+/') , '-', $string);
      return strtolower(trim($string, '-'));
  }
  //And that's it. I hope you had fun reading and understanding this code. To be honnest, it's not that hard
  //Now you can leave your seat, go get a drink, tell your boss of your new accomplishment, whatever you want, you're free!!

}















//Why are you here? the code already ended...






























//like really, you won't find anything...






























//Dude....


























































/*
░░░░░▄▄▄▄▀▀▀▀▀▀▀▀▄▄▄▄▄▄░░░░░░░
░░░░░█░░░░▒▒▒▒▒▒▒▒▒▒▒▒░░▀▀▄░░░░
░░░░█░░░▒▒▒▒▒▒░░░░░░░░▒▒▒░░█░░░
░░░█░░░░░░▄██▀▄▄░░░░░▄▄▄░░░░█░░
░▄▀▒▄▄▄▒░█▀▀▀▀▄▄█░░░██▄▄█░░░░█░
█░▒█▒▄░▀▄▄▄▀░░░░░░░░█░░░▒▒▒▒▒░█
█░▒█░█▀▄▄░░░░░█▀░░░░▀▄░░▄▀▀▀▄▒█
░█░▀▄░█▄░█▀▄▄░▀░▀▀░▄▄▀░░░░█░░█░
░░█░░░▀▄▀█▄▄░█▀▀▀▄▄▄▄▀▀█▀██░█░░
░░░█░░░░██░░▀█▄▄▄█▄▄█▄████░█░░░
░░░░█░░░░▀▀▄░█░░░█░█▀██████░█░░
░░░░░▀▄░░░░░▀▀▄▄▄█▄█▄█▄█▄▀░░█░░
░░░░░░░▀▄▄░▒▒▒▒░░░░░░░░░░▒░░░█░
░░░░░░░░░░▀▀▄▄░▒▒▒▒▒▒▒▒▒▒░░░░█░
░░░░░░░░░░░░░░▀▄▄▄▄▄░░░░░░░░█░░
*/
?>
