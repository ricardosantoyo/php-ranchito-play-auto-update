# Presumiendo México Auto Updater - Mesa para Todos#

Actualizaciones automáticas por parte de un RSS en Endavo para uso de Presumiendo México de la sección de noticias.

### ¿Para qué sirve? ###

* Actualizar automáticamente el contenido de Presumiendo México por medio de un RSS y API de Endavo, únicamente videos de Mesa para Todos.

### ¿Cómo configurarlo? ###

* Descargar los archivos al servidor
* Cambiar el Client Key y Secret Key en el archivo endavo.php
* Cambiar el nombre de usuario y contraseña de la base de datos en BDConnection.php
* Si hay cambios en las categorías, agregarlas o modificarlas en el archivo index.php

### Propiedad ###

* Telemetrika S. A. de C. V.
* Ing. Ricardo Santoyo Reyes, Telemetrika