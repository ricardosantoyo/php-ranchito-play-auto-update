<?php
require 'endavo.php';
require 'BDConnection.php';

$ENDAVO = new RanchitoPlayAutoUpdate();

$array = $ENDAVO->getLatest();
$array_compared = $ENDAVO->compareLatest($array);
if($array_compared["status"] == "ok"){
  $ENDAVO->insertLatest($array_compared);
}

//Welcome to the Auto Update code for Ranchito Play!!
//Please take a seat and remember to keep your arms and legs in your desk all the time.
//Hope you understand what I did, I think I made it simple but that's because I know what I did.
//If you want help or dont understand something... may god have mercy on you.

//Remember to always output text in Spanish.
class RanchitoPlayAutoUpdate{
  public $latest = NULL;
  public $json_endavo;

  public function __construct() {

	}

  function getLatest(){
    //We connect to the RSS file to get the latest 15 videos.
    $url = "http://ultratelecom.endavomedia.com/rss/custom/7570/";
    $latest = file_get_contents($url);
    $x = new SimpleXmlElement($latest);

    //And we turn them to a really pretty JSON
    $json = json_encode($x, JSON_UNESCAPED_UNICODE);

    $json_endavo = array(
      "status" => "ok",
      "desc" => "Full list of rss entries"
    );

    try{
      //We get the object form the JSON to use it a little easier.
      $json_obj = json_decode($json, true);
      foreach($json_obj["channel"]["item"] as $e){
        $exp_id = explode("-", $e["guid"]);
        $id = $exp_id[0];

        //Okay, now we connect to the endavo API in order to get the data from the RSS items
        $endavo_api = new EmmsApiClientTest();

        //This is where the call is being made, the method is called 'test', I'm too lazy to change its name...
        $result = $endavo_api->test($id);

        if($result == NULL){
          return array("status" => "err/video", "desc" => "Hubo un error al obtener los datos de los videos");
        } else if($result["status"] == "ok"){

          //We save the endavo items to an array
          $json_endavo["results"][] = $result["result"];

        }

      }

      return $json_endavo;

    } catch(Exception $e){
      $err = array("status" => "err/entries", "desc" => "Ocurrió un error al intentar obtener las últimas entradas en el RSS.");

      $this->saveLog($err);

      return $err;
    }
  }

  function compareLatest($array){
    //echo "<pre>" . json_encode($array, JSON_UNESCAPED_UNICODE) . "</pre>";

    $_BD= new connection();

    $conn = $_BD->connect();

    if($conn == NULL){
      //Couldn't connect to the database, show error
      $result = array(
        "status" => "err/db",
        "desc" => "Error al intentar conectarse a la base de datos"
      );

      $this->saveLog($result);

      return $result;
    }

    $result = array(
      "status" => "ok",
      "desc" => "Lista de entradas ya comparadas"
    );

    foreach($array["results"] as $item){
      //ONLY current year videos
      //That's because this code was made when we already had a SHIT-TON of videos on the site from last year (2016)
      //and none of them had the '_endavo_id' on their meta data
      $year = date("Y");
      if(strpos($item["orderDateTime"], $year) !== false){

        try{
          //Okay, we must compare if the videos from the RSS are already inserted.
          //If '_endavo_id' isn't in the DB, then it adds the item to an array.
          $query = "SELECT * FROM wp_postmeta WHERE meta_key='_endavo_id' AND meta_value=".$item["id"].";";

          $res = $conn->prepare($query);
          $res->execute();

          //If there are any result sets, then save them
          if($res->rowCount() == 0){
            $result["results"][] = $item;
          }

        } catch (PDOException $e){
          $result = array(
            "status" => "err",
            "desc" => $e->getMessage()
          );

          return $result;
        }
      }
    }

    return $result;
  }

  function insertLatest($array){
    $_BD= new connection();

    $conn = $_BD->connect();

    $result = array("status" => "ok");

    try{

      //We must compare if there are entries or nah.
      //Why would you want to insert a video if there are not videos to insert?
      if(!array_key_exists("results", $array)){
        $timezone  = -6;
        $date = gmdate("Y-m-d H:i:s", time() + 3600*($timezone+date("I")));
        $result["date"] = $date;
        $result["desc"] = "No hay nuevas entradas. Ejecución terminada.";

        $this->saveLog($result);

        echo "<pre>" . json_encode($result, JSON_UNESCAPED_UNICODE) . "</pre>";

        return;
      }

      //We insert each item on the DB, this is where the magic happens
      $item_index = 0;
      foreach($array["results"] as $item){

        //IMPORTANT: Check if the item isn't an audio file
        if($item["type"] == "audio"){
          $item_index++;
          $timezone  = -6;
          $date = gmdate("Y-m-d H:i:s", time() + 3600*($timezone+date("I")));
          $result["status"] = "err/audio";
          $result["date"] = $date;
          $result["desc"] = "Archivo de audio encontrado, se omitirá. Endavo ID: ".$item["id"];

          $this->saveLog($result);

          echo "<pre>" . json_encode($result, JSON_UNESCAPED_UNICODE) . "</pre>";

          if($item_index == count($array["results"])) {
            $result = array();
            $result["status"] = "ok";
            $result["date"] = $date;
            $result["desc"] = "No hay nuevas entradas. Ejecución terminada.";

            $this->saveLog($result);

            echo "<pre>" . json_encode($result, JSON_UNESCAPED_UNICODE) . "</pre>";

            return;
          }

          continue;
        }

        $slug = $this->friendly_url($item["slug"]);

        $non_chars = array('\'', '"', '\\\'', '\\"', '\\');
        $item_title = str_replace($non_chars, "", $item["title"]);
        $item_description = str_replace($non_chars, "", $item["description"]);

        //IMPORTANT: The script for the video to show.
        //IMPORTANTER: Don't change it unless you know what you are doing.
        $adaptive = json_encode($item["adaptive"][0]);
        $adaptive = json_decode($adaptive, true);
        $adaptive = $adaptive["url"];

        $renditions = json_encode($item["renditions"][count($item["renditions"] )- 1]);
        $renditions = json_decode($renditions, true);
        $renditions = $renditions["hlsUrl"];

        $video_file = $adaptive == "" ? $renditions : $adaptive;

        $tm_video_code = "<div id=\"PMTV\"></div>\n".
          "<script type=\"text/javascript\">\n".
          "jwplayer(\"PMTV\").setup({\n".
            "playlist: [{\n".
              "file: \"" . $video_file . "\",\n".
              "title: \"" . addslashes( $item_title ) . "\"\n".
            "}],\n".
            "advertising: {\n".
              "client: \"googima\", \n".
              "\"skipoffset\": 10, \n".
              "tag: \"https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/32830837/Portal_Radio-Ranchito_Video&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&url=[referrer_url]&description_url=[description_url]&correlator=[timestamp]\"\n".
            "}, \n".
            "primary: \"flash\", \n".
            "autostart: \"true\", \n".
            "width: \"100%\", \n".
            "aspectratio: \"16:9\", \n".
            "sharing: {\n".
              "sites: [\"facebook\",\"twitter\",\"tumblr\"]\n".
            "},\n".
            "androidhls: \"true\"\n".
          "});\n".
          "</script>\n";

        $query = 'INSERT INTO wp_posts '.
            '(post_author, post_date, post_date_gmt, post_content, post_title, post_excerpt, post_status, comment_status, ping_status, post_name, to_ping, pinged, post_modified, post_modified_gmt, post_content_filtered, post_parent, guid, menu_order, post_type, comment_count) '.
            'VALUES '.
            '(3,'.//post_author: Author, 3 means Mesa para Todos
              '"'.$item["orderDateTime"].'",'.//post_date: Date of publication
              '"'.$item["orderDateTime"].'",'.//post_date_gmt: GMT date of publication
              '\'' . $tm_video_code . '\','.//post_content: Description
              '"'. addslashes( $item_title ) .'",'.//post_title: Title of the video
              '"",'.//post_excerpt: Excerpt, we don't handle them
              '"publish",'.//post_status: Status to publish it
              '"closed",'.//comment_status: Can comment on video
              '"closed",'.//ping_status: Can access it by ping
              '"'.$slug.'",'.//post_name: Slug for the video entry
              '"",'.//to_ping: Not important
              '"",'.//pinged: Not important
              '"'.$item["orderDateTime"].'",'.//post_modified: Date of the last modification
              '"'.$item["orderDateTime"].'",'.//post_modified_gmt: Date of the last modification GMT
              '"",'.//post_content_filtered: Not important
              '0,'.//post_parent: Parent of the post, mostly 0
              '"",'.//guid: URL access to the post, will be updated next with the post ID
              '0,'.//menu_order: Always 0
              '"portfolio_item",'.//post_type: Could be 'attachment' for images, but let's keep it as 'post'
              '0'.//comment_count: How many comments the post has? always 0 because nobody ever comments. (just kidding, please don't fire me)
            ');';

        $res = $conn->prepare($query);
        $res->execute();

        //We update the guid of the entry with the ID from the insertion
        $id = $conn->lastInsertId();
        $query = "UPDATE wp_posts SET guid='http://presumiendomexico.com.mx/portfolio_item/".$slug."' WHERE ID=".$id.";";
        $conn->exec($query);

        //We copy the image to the server
        $imageRes["status"] = "err/image";
        if(array_key_exists("images", $item)){
          $imageRes = $this->copyImage($item["images"]);
        }

        //Then we insert the meta data for each post
        $timezone  = -6;
        $date = gmdate("Y-m-d H:i:s", time() + 3600*($timezone+date("I")));
        $result["date"] = $date;

        //And we insert the image data to a new post
        if($imageRes["status"] == "ok"){
          $image_flag = $this->insertPostImage($imageRes, $id);
        }

        if($this->insertPostmeta($conn, $id, $item)){
          $result["results"][] = array("item-id" => $id, "item-endavo-id" => $item["id"], "postmeta" => "success", "image" => $image_flag);
        } else {
          $result["results"][] = array("item-id" => $id, "item-endavo-id" => $item["id"], "postmeta" => "error", "image" => $image_flag);
        }

      }

      $this->saveLog($result);

      echo "<pre>" . json_encode($result, JSON_UNESCAPED_UNICODE) . "</pre>";

    } catch (PDOException $e){
      $timezone  = -6;
      $date = gmdate("Y-m-d H:i:s", time() + 3600*($timezone+date("I")));

      $err = array(
        "status" => "err/item",
        "date" => $date,
        "desc" => $e->getMessage()
      );

      $this->saveLog($err);

      echo "<pre>" . json_encode($err, JSON_UNESCAPED_UNICODE) . "</pre>";
      return;
    }
  }

  /**
  * Function to insert image's post and postmeta data to the DB
  **/
  function insertPostImage($imageRes, $id){
    $aux_img = json_encode($imageRes);
    $imageRes = json_decode($aux_img, true);

    $_BD= new connection();

    $conn = $_BD->connect();

    $image = $imageRes["results"];

    //In order to display the image we need to know which is its metatype and save it respectively.
    if($image["extension"] == "jpg"){
      $mime_type = "jpeg";
    } else {
      $mime_type = $image["extension"];
    }

    try{

      $timezone  = -6;
      $date = gmdate("Y-m-d H:i:s", time() + 3600*($timezone+date("I")));

      //This is where we insert the image data as a post, later we have to insert its meta data
      $query = 'INSERT INTO wp_posts '.
          '(post_author, post_date, post_date_gmt, post_content, post_title, post_excerpt, post_status, comment_status, ping_status, post_name, to_ping, pinged, post_modified, post_modified_gmt, post_content_filtered, post_parent, guid, menu_order, post_type, post_mime_type, comment_count) '.
          'VALUES '.
          '(1,'.//post_author: Author, 1 means Ricardo
            '"'.$date.'",'.//post_date: Date of publication
            '"'.$date.'",'.//post_date_gmt: GMT date of publication
            '"",'.//post_content: Description, not important here
            '"'.$image["name"].'",'.//post_title: Title of the video
            '"",'.//post_excerpt: Excerpt, we don't handle them
            '"inherit",'.//post_status: Status to publish it
            '"open",'.//comment_status: Can comment on video
            '"closed",'.//ping_status: Can access it by ping
            '"'.$image["name"].'",'.//post_name: Slug for the video entry
            '"",'.//to_ping: Not important
            '"",'.//pinged: Not important
            '"'.$date.'",'.//post_modified: Date of the last modification
            '"'.$date.'",'.//post_modified_gmt: Date of the last modification GMT
            '"",'.//post_content_filtered: Not important
            '0,'.//post_parent: Parent of the post, mostly 0
            '"http://presumiendomexico.com.mx/wp-contents/uploads/endavo/'.$image["filename"].'",'.//guid: URL access to the image
            '0,'.//menu_order: Always 0
            '"attachment",'.//post_type: Could be 'attachment' for images, but let's keep it as 'post'
            '"image/'.$mime_type.'",'.//post_mime_type: image/+extension
            '0'.//comment_count: How many comments the post has? always 0 because nobody ever comments. (just kidding, please don't fire me)
          ');';
          //Did you notice I just copy-pasted the query from the one on the post? LOL

      $res = $conn->prepare($query);
      $res->execute();
      $imageId = $conn->lastInsertId();

      //The meta data of the image. I'm not quite sure what this means, but if you don't know what you are doing, leave it intact.
      $att_meta = 'a:5:{s:5:"width";i:500;s:6:"height";i:500;s:4:"file";s:24:"endavo/'.$image["filename"].'";s:5:"sizes";a:13:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"endavo/'.$image["filename"].'";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"endavo/'.$image["filename"].'";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"videopro_misc_thumb_1";a:4:{s:4:"file";s:22:"endavo/'.$image["filename"].'";s:5:"width";i:50;s:6:"height";i:50;s:9:"mime-type";s:10:"image/jpeg";}s:21:"videopro_misc_thumb_2";a:4:{s:4:"file";s:23:"endavo/'.$image["filename"].'";s:5:"width";i:100;s:6:"height";i:75;s:9:"mime-type";s:10:"image/jpeg";}s:21:"videopro_misc_thumb_3";a:4:{s:4:"file";s:24:"endavo/'.$image["filename"].'";s:5:"width";i:205;s:6:"height";i:115;s:9:"mime-type";s:10:"image/jpeg";}s:21:"videopro_misc_thumb_4";a:4:{s:4:"file";s:24:"endavo/'.$image["filename"].'";s:5:"width";i:277;s:6:"height";i:156;s:9:"mime-type";s:10:"image/jpeg";}s:21:"videopro_misc_thumb_5";a:4:{s:4:"file";s:24:"endavo/'.$image["filename"].
        '";s:5:"width";i:298;s:6:"height";i:298;s:9:"mime-type";s:10:"image/jpeg";}s:21:"videopro_misc_thumb_6";a:4:{s:4:"file";s:24:"endavo/'.$image["filename"].'";s:5:"width";i:320;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:21:"videopro_misc_thumb_7";a:4:{s:4:"file";s:24:"endavo/'.$image["filename"].'";s:5:"width";i:407;s:6:"height";i:229;s:9:"mime-type";s:10:"image/jpeg";}s:21:"videopro_misc_thumb_8";a:4:{s:4:"file";s:24:"endavo/'.$image["filename"].'";s:5:"width";i:500;s:6:"height";i:318;s:9:"mime-type";s:10:"image/jpeg";}s:21:"videopro_misc_thumb_9";a:4:{s:4:"file";s:24:"endavo/'.$image["filename"].'";s:5:"width";i:500;s:6:"height";i:358;s:9:"mime-type";s:10:"image/jpeg";}s:22:"videopro_misc_thumb_10";a:4:{s:4:"file";s:24:"endavo/'.$image["filename"].'";s:5:"width";i:500;s:6:"height";i:450;s:9:"mime-type";s:10:"image/jpeg";}s:14:"tptn_thumbnail";a:4:{s:4:"file";s:24:"endavo/'.$image["filename"].
          '";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}';

      //We insert the meta data with the ID of the image post, and the ID of the original post with the thumbnail ID.
      $query = 'INSERT INTO wp_postmeta (post_id, meta_key, meta_value) VALUES '.
          '("'.$imageId.'", "_wp_attached_file", "endavo/'.$image["filename"].'"), '.
          '("'.$imageId.'", "_wp_attachment_metadata", \''.$att_meta.'\'), '.
          '("'.$id.'", "_thumbnail_id", "'.$imageId.'");';


      $res = $conn->prepare($query);
      $res->execute();

      return true;
    } catch (PDOException $e){
        $timezone  = -6;
        $date = gmdate("Y-m-d H:i:s", time() + 3600*($timezone+date("I")));

        $err = array(
          "status" => "err/image",
          "date" => $date,
          "desc" => $e->getMessage()
        );

        $this->saveLog($err);

        echo "<pre>" . json_encode($err, JSON_UNESCAPED_UNICODE) . "</pre>";
        return false;
    }
  }


  function insertPostmeta($conn, $id, $item){
    try{
      //$tm_video_code = "<script type=\"text/javascript\">\nvar _embedPlayerParams = _embedPlayerParams || [];\n_embedPlayerParams[\"_mediaHash\"] = \"".$item["hash"]."\";\n_embedPlayerParams[\"_clientId\"] = \"bpktatpfXvv2wjV3MBew3lncNkMW6OpR\";\n_embedPlayerParams[\"_width\"] = \"auto\";\n_embedPlayerParams[\"_height\"] = \"auto\";\n_embedPlayerParams[\"_autoPlay\"] = 0;\n</script>\n<script src=\"//player.endavomedia.com/show.js\" type=\"text/javascript\"></script>";
      //$tm_video_file = $item["video"];

      $query = 'INSERT INTO wp_postmeta (post_id, meta_key, meta_value) VALUES '.
        '('.$id.', "_edit_lock", "1482860667:2"),'.//Couldn't decipher what this number means, but it's always this
        '('.$id.', "_edit_last", "1"),'.
        '('.$id.', "_yoast_wpseo_primary_portfolio_category", "26"),'.
        '('.$id.', "identity_port_displaymode", "lightbox_image"),'.
        '('.$id.', "identity_pagebg_color", "#FFFFFF"),'.
        '('.$id.', "identity_pagehead_color", "#333"),'.
        '('.$id.', "identity_pageheadbg_color", "#FFF"),'.
        '('.$id.', "_yoast_wpseo_content_score", "30"),'.
        '('.$id.', "_endavo_id", "'.$item["id"].'")';

      $res = $conn->prepare($query);
      $res->execute();

      //Okay, we reached the final part of the meta insertion, you should be happy.
      //We must compare categories because the channels of endavo and the wordpress categories are different
      //so we have to know which one is which.
      $i = 0;
      foreach($item["channels"] as $channel){
        if($i == 1){
          break;
        }

        $aux = json_encode($channel);
        $category_obj = json_decode($aux, true);

        //IMPORTANT: Category 27 for MPT
        $category = "26";
        //telemijt_up_mpt
        //gl-[qh-}y~ig

        $i++;
      }

      $query = "INSERT INTO wp_term_relationships (object_id, term_taxonomy_id, term_order) ".
               "VALUES (" . $id . ", " . $category . ", 0);";

      $res = $conn->prepare($query);
      $res->execute();

      return true;
    } catch (PDOException $e){
      $err = array(
        "status" => "err/postmeta",
        "desc" => "Error insertar postmeta/category: " . $e->getMessage()
      );

      $this->saveLog($err);

      echo "<pre>" . json_encode($err, JSON_UNESCAPED_UNICODE) . "</pre>";
      return false;
    }
  }

  //You thought it was the last function?.. SIKE!
  //Here's where we copy the image to the page server, this is because wordpress won't recognize
  //images from an external server.
  function copyImage($array){
    $aux = 0;
    $url = "";
    foreach($array as $image){
      $aux_img = json_encode($image);
      $item = json_decode($aux_img, true);

      //We choose the image with best resolution
      if(intval($item["width"]) > $aux){
        $aux = intval($item["width"]);
        $url = $item["url"];
      }
    }

    if($url == "") { return; };

    //With pathinfo we can get the file's meta data, but we are interested in the extension
    $info = pathinfo($url);

    //With basename we get the filename without the extension
    $filename = basename($url, ".".$info["extension"]);

    //The path where we are going to copy the new file
    $path = "../wp-content/uploads/endavo/".$filename.".".$info["extension"];
    if(!@copy($url, $path)){
      //If there's an error, we save the log
      $timezone  = -6;
      $date = gmdate("Y-m-d H:i:s", time() + 3600*($timezone+date("I")));

      $err = array(
        "status" => "err/image",
        "date" => $date,
        "results" => array(
          "desc" => "No se ha podido copiar la imagen al servidor.",
          "url" => $url,
          "path" => $path,
          "error" => error_get_last()
        ),
        "imageUrl" => $url
      );

      $this->saveLog($err);

      return $err;
    } else {
      //We return the array with the data of the image to insert it in the DB
      $res = array(
        "status" => "ok",
        "results" => array(
          "url" => $url,
          "filename" => $filename.".".$info["extension"],
          "name" => $filename,
          "extension" => $info["extension"]
        ),
        "desc" => "Imagen copiada satisfactoriamente"
      );

      return $res;
    }
  }

  function saveLog($array){
    $timezone  = -6;
    $date = gmdate("Y-m-d H:i:s", time() + 3600*($timezone+date("I")));

    $_BD= new connection();

    $conn = $_BD->connect();

    if(array_key_exists("results", $array)){
      $content = $array["results"];
    } else {
      $content = $array["desc"];
    }

    $non_chars = array('\'', '"', '\\\'', '\\"');
    $new_content = str_replace($non_chars, "", json_encode($content, JSON_UNESCAPED_UNICODE));

    $query = "INSERT INTO wp_endavo_log (log_date, status, content) VALUES ('".$date."', '".$array["status"]."', '".$new_content."');";

    $res = $conn->prepare($query);
    $res->execute();
  }

  function friendly_url($string){
      $string = str_replace(array('[\', \']'), '', $string);
      $string = preg_replace('/\[.*\]/U', '', $string);
      $string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '-', $string);
      $string = htmlentities($string, ENT_COMPAT, 'utf-8');
      $string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string );
      $string = preg_replace(array('/[^a-z0-9]/i', '/[-]+/') , '-', $string);
      return strtolower(trim($string, '-'));
  }
  //And that's it. I hope you had fun reading and understanding this code. To be honnest, it's not that hard
  //Now you can leave your seat, go get a drink, tell your boss of your new accomplishment, whatever you want, you're free!!

}















//Why are you here? the code already ended...






























//like really, you won't find anything...






























//Dude....


























































/*
░░░░░▄▄▄▄▀▀▀▀▀▀▀▀▄▄▄▄▄▄░░░░░░░
░░░░░█░░░░▒▒▒▒▒▒▒▒▒▒▒▒░░▀▀▄░░░░
░░░░█░░░▒▒▒▒▒▒░░░░░░░░▒▒▒░░█░░░
░░░█░░░░░░▄██▀▄▄░░░░░▄▄▄░░░░█░░
░▄▀▒▄▄▄▒░█▀▀▀▀▄▄█░░░██▄▄█░░░░█░
█░▒█▒▄░▀▄▄▄▀░░░░░░░░█░░░▒▒▒▒▒░█
█░▒█░█▀▄▄░░░░░█▀░░░░▀▄░░▄▀▀▀▄▒█
░█░▀▄░█▄░█▀▄▄░▀░▀▀░▄▄▀░░░░█░░█░
░░█░░░▀▄▀█▄▄░█▀▀▀▄▄▄▄▀▀█▀██░█░░
░░░█░░░░██░░▀█▄▄▄█▄▄█▄████░█░░░
░░░░█░░░░▀▀▄░█░░░█░█▀██████░█░░
░░░░░▀▄░░░░░▀▀▄▄▄█▄█▄█▄█▄▀░░█░░
░░░░░░░▀▄▄░▒▒▒▒░░░░░░░░░░▒░░░█░
░░░░░░░░░░▀▀▄▄░▒▒▒▒▒▒▒▒▒▒░░░░█░
░░░░░░░░░░░░░░▀▄▄▄▄▄░░░░░░░░█░░
*/
?>
