<?php
require ("BDConnection.php");

function getRecents(){
  $_BD= new connection();

  $conn = $_BD->connect();

  try{
    $limit = (isset($_GET["limit"])) ? $_GET["limit"] : "4";
    $query = 'SELECT DISTINCT wp_ranchxtspostmeta.post_id AS ID, wp_ranchxtspostmeta.meta_value AS thumbnail, wp_ranchxtsposts.post_title, wp_ranchxtsposts.guid FROM wp_ranchxtspostmeta, wp_ranchxtsposts WHERE wp_ranchxtspostmeta.meta_key like "_thumbnail_id" AND wp_ranchxtspostmeta.post_id=wp_ranchxtsposts.ID ORDER BY wp_ranchxtspostmeta.post_id desc LIMIT '.$limit;

    $res = $conn->prepare($query);
    $res->execute();

    $result = array(
      "status" => "ok"
    );

    if($res->rowCount() != 0){
      foreach ($res as $item) {
        $thumb = $item["thumbnail"];

        $query = "SELECT guid FROM wp_ranchxtsposts WHERE ID = " . $thumb . ";";

        $tmb = $conn->prepare($query);
        $tmb->execute();

        if($tmb->rowCount() != 0){
          foreach ($tmb as $thumbnail) {
            $result["results"][] = array(
              "title" => $item["post_title"],
              "url" => $item["guid"],
              "imgUrl" => $thumbnail["guid"]
            );
          }
        } else {
          $result["results"][] = array(
            "title" => $item["post_title"],
            "url" => $item["guid"]
          );
        }

      }

      var_dump($result);

      return json_encode($result, JSON_PRETTY_PRINT);
    }
  } catch (PDOException $e){

    $err = array(
      "status" => "err",
      "desc" => "Error al conectarse a la base de datos"
    );

    echo json_encode($err, JSON_PRETTY_PRINT);
    return;
  }
}

echo getRecents();
 ?>
