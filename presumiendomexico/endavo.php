<?php
require 'Communication.php';

class EmmsApiClientTest
{
	protected $properties = array(
	'user_agent'            => 'Ultra Test client',
	'api_accept' => 'Accept: application/json',
	'api_version' => 'v1',
	'api_base_url' => 'https://api-usaeast.endavomedia.com/api',
	'api_oauth_base_url' => 'https://api-usaeast.endavomedia.com/oauth2',
	'api_upload_base_url' => 'http://uploads-staging.endavomedia.com',
	'api_oauth_client_id' => 'dCRNT7r3rPuPjVktlkdOeP9zX1KlSrR1',
	'api_oauth_client_secret' => '4XgyAQLa7IJZ60NoBIBeAj358nF026QW',
	'connect_timeout'		=> 30,
	'verify_ssl'			=> false,
	);

	public $comm;

	public function __construct() {
		$this->comm	= new EmmsApiClient($this->properties);
	}

	public function test($videoId) {
		// get token
		$jsonToken = $this->comm->callEmmsApi($this->properties['api_oauth_base_url'] . '/token', array(
		'grant_type' => 'client_credentials',
		'client_id' => $this->properties['api_oauth_client_id'],
		'client_secret' => $this->properties['api_oauth_client_secret'],
		), 'post', array(), "");

		//print ("Token result -> \n<br/>").var_dump($jsonToken);
		$tokenArray = json_decode($jsonToken['data'], true);
		//var_dump($tokenArray);

		//Read Playlist
		//$videoId = $id;
		/*if(isset($_GET["id"])){
			$videoId = $_GET["id"];
		} else {
			die(json_encode(array("err"=>"404","descripcion"=>"Página no encontrada")));
		}*/
		$parameterArray = array(
		  'limit' => 20
		);
		//$parameterArray = array();
		$result = $this->comm->callEmmsApi($this->properties['api_base_url'] . '/'.$this->properties['api_version'].'/media/'.$videoId, $parameterArray, 'get', array(), $tokenArray['access_token']);

		//print ("Playlist result -> \n<br/>").var_dump($result);
		$resultArray = json_decode($result['data']);
		//print("<pre>").print_r($resultArray)."</pre>";

		$arra = array(
			 "status"=>"ok",
			 "result" => array(
				 "id"=>$resultArray->media->id,
				 "title"=>$resultArray->media->title,
				 "type"=>$resultArray->media->type,
				 "slug"=>$resultArray->media->urlSlug,
				 "description"=>$resultArray->media->description,
				 "category"=>$resultArray->media->category,
				 "channels"=>$resultArray->media->channels,
				 "hash"=>$resultArray->media->hash,
				 "orderDateTime"=>$resultArray->media->orderDateTime,
				 "images"=>$resultArray->media->images,
				 "adaptive"=>$resultArray->media->adaptiveRenditions,
				 "renditions"=>$resultArray->media->renditions
			 )
		);

/*
		echo "<pre>";
		echo json_encode($arra, JSON_PRETTY_PRINT);
		echo "</pre>";*/

		return $arra;

	}

}

/*$client = new EmmsApiClientTest();
$client->test();*/

?>
