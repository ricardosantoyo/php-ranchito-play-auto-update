<?php

$static_category = "Ultra Noticias - Michoacán";

$json_categories = file_get_contents("categories.json");

$JSON_ITERATOR = new RecursiveIteratorIterator(
  new RecursiveArrayIterator(json_decode($json_categories, true)),
  RecursiveIteratorIterator::SELF_FIRST
);

echo "<pre>";

$category = 1;

foreach ($JSON_ITERATOR as $key => $val) {
  if ($key === $static_category){
    $category = $val["category"];
  }
}

echo $category;

echo "</pre>";
