<?php
	/******************
	* EmmsApiClient Class
	* Uses CURL to communitcate with EMMS API.  This function prints several items to the HTML response.
	******************/

	class EmmsApiClient
	{
		/******************
		* This array holds the options that are used during the cURL call to the API server. So
		*
		* @properties['api_base_url']				string 	Set and passed by driver script
		* @properties['api_oauth_base_url']			string 	Set and passed by driver script
		* @properties['api_oauth_client_id']		string 	Set and passed by driver script
		* @properties['api_oauth_client_secret']	string 	Set and passed by driver script
		* @properties['api_version']					string 	Set and passed by driver script
		* @properties['user_agent']					string 	User Agent field value
		* @properties['api_accept']					string 	Set and passed by driver script
		* @properties['connect_timeout']			integer Connect timeout setting
		* @properties['verify_ssl']					string 	Verify the SSL certificate
		******************/
		protected $properties = array(
			'api_accept' => 'Accept: application/json',
			'connect_timeout'		=> 30,
			'verify_ssl'			=> false,
		);

		/******************
		* EmmsApiClient Constructor
		* Creates full properties array with values passed from the driver.
		*
		* @param array $properties - Custom properties from driver
		******************/
		public function __construct(array $properties)
		{
			foreach (array('api_base_url', 'api_oauth_base_url', 'api_oauth_client_id', 'api_oauth_client_secret') as $name)
			{
				if (!isset($properties[$name]))
				{
					throw new Exception("$name property is not set.");
				}
			}
			$this->properties = array_merge($this->properties, $properties);
		}

		/******************
		* callEmmsApi
		* Uses CURL to communitcate with EMMS API.  This function prints several items to the HTML response.
		*
		* @param string $url 			Target resource
		* @param array 	$params 		Request parameter array - All request parameters to be sent to the API are included in this array
		* @param array 	$requestHeaders	Request header information - Can be used to added additional headers to the request
		* @param string	$method			HTTP Method to use - Values: post, get, put, delete
		* @param string	$token 			EMMS generated access token
		*
		* @return array					Array with response headers, response body, and request information
		******************/
		public function callEmmsApi($url, $params = array(), $method = 'get', $requestHeaders = array(), $token = "")
		{
			/******************
			* Initialize CURL object
			******************/
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,  $this->properties['connect_timeout']);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->properties['verify_ssl']);
			curl_setopt($ch, CURLOPT_USERAGENT, $this->properties['user_agent']);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 	// Follow redirects recursively
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);	// Return the data
			curl_setopt($ch, CURLOPT_HEADER, 1);			// Return the response headers

			/******************
			* Add items to request headers array
			* Set up HTTP method and HTTP Request Type
			******************/
			$requestHeaders[] = $this->properties['api_accept'];

			if (in_array($method, array('get', 'put', 'post', 'delete')))
			{
				switch ($method)
				{
					case 'get':
						$requestBodyParmeters = "No Request Body Parameters In GET Request";
						if ( !empty($params) ) {
							$url=$url."?".http_build_query($params);
						}
						curl_setopt($ch, CURLOPT_URL, $url);
						curl_setopt($ch, CURLOPT_POST, false);
						break;
					case 'delete':
						$requestBodyParmeters = "No Request Body Parameters In DELETE Request";
						if ( !empty($params) ) {
							$url=$url."?".http_build_query($params);
						}
						curl_setopt($ch, CURLOPT_URL, $url);
						curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
						curl_setopt($ch, CURLOPT_POSTFIELDS, false);
						break;
					case 'put':
						if ( !empty($params) ) {
							$requestBodyParmeters = http_build_query($params);
						}
						$requestHeaders[] = 'Content-Type: application/x-www-form-urlencoded';
						curl_setopt($ch, CURLOPT_URL, $url);
						curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
						curl_setopt($ch, CURLOPT_POSTFIELDS, $requestBodyParmeters);
						break;
					case 'post':
						if ( !empty($params) ) {
							$requestBodyParmeters = http_build_query($params);
						}
						$requestHeaders[] = 'Content-Type: application/x-www-form-urlencoded';
						curl_setopt($ch, CURLOPT_URL, $url);
						curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
						curl_setopt($ch, CURLOPT_POSTFIELDS, $requestBodyParmeters);
						break;
					default:
						die('Method not allowed.');
						break;
				}
			}

			/******************
			* Add EMMS generated access token to request header array, if it was passed
			******************/
			if ($token != "" ) {
				$requestHeaders[] = 'Authorization: Bearer ' . $token;
			}

			/******************
			* Add request headers to request
			******************/
			if (count($requestHeaders) > 0)	{
				curl_setopt($ch, CURLOPT_HTTPHEADER, $requestHeaders);
			}

			/******************
			* Execute request and gather timing stats on call
			******************/
			$html = curl_exec($ch);
			$info = curl_getinfo($ch);

			if (!isset($html) || $html == null) {
				return false;
			}

			/******************
			* Parse response into array of response headers (headers), response data (html), and call timing (info)
			******************/
			//$proxyResponseOffset=$this->properties['proxy_response_offset'];
			$data = array(
				'requestMethod'			=> $method,
				'requestURL'			=> $url,
				'requestHeaders'		=> $requestHeaders,
				'requestBodyParameters'	=> $requestBodyParmeters,
				'fullResponse'			=> $html,
				'headers'				=> substr($html, 0, $info['header_size'] ),
				'data'					=> substr($html, $info['header_size'] ),
				'info'					=> $info,
			);

			return $data;
		}
	}
?>
