<?php
require 'BDConnection.php';

$_BD= new connection();

$conn = $_BD->connect();

if($conn == NULL){
  //Couldn't connect to the database, show error
  $result = array(
    "status" => "err",
    "desc" => "Error al intentar conectarse a la base de datos"
  );

  return "<pre>" . json_encode($result, JSON_PRETTY_PRINT) . "</pre>";
}

try{
  $query = "SELECT * FROM wp_ranchxtspostmeta WHERE meta_key='_endavo_id';";

  $res = $conn->prepare($query);
  $res->execute();

  if($res->rowCount() != 0){

    $array = $res->fetchAll();

    foreach ($array as $item) {
      $post_id = $item["post_id"];

      $query = "SELECT meta_value as Script FROM wp_ranchxtspostmeta WHERE post_id=" . $post_id . " AND meta_key='tm_video_code';";

      $res = $conn->prepare($query);
      $res->execute();

      $post = $res->fetchAll();

      $html_script = $post[0]["Script"];

      echo "<pre><hr><h3>BEFORE</h3>";
      echo htmlspecialchars($html_script);
      echo "</pre>";

      $has_cast = strpos($html_script, "cast: {},");

      if($has_cast === false || $has_cast === 0){
        $has_script_first = strpos($html_script, "<script");

        echo "<h1> INDEX SCRIPT: " . $has_script_first . "</h1>";

        if($has_script_first != 0){
          $index = strpos($html_script, "\",");
          $html_script_after = substr_replace($html_script, ",\ncast: {},", $index, strlen("\","));

          //$index = strpos($html_script, "id=\"v ");
          //$html_script_after = substr_replace($html_script, "", $index, strlen("id=\"v "));

          echo "<pre><h3>AFTER</h3>";
          echo htmlspecialchars($html_script_after);
          echo "</pre>";
          echo "<hr>";

          //UPDATE
          /*$query = "UPDATE wp_ranchxtspostmeta SET meta_value='" . $html_script_after . "' WHERE post_id=" . $post_id . " AND meta_key='tm_video_code';";

          $res = $conn->prepare($query);
          $res->execute();*/
        }

      } else {
        echo "Already has CAST";
        echo "<hr>";
      }
    }

  } else {
    $result = array(
      "status" => "err",
      "desc" => "Error al intentar seleccionar items en BD"
    );

    return "<pre>" . json_encode($result, JSON_PRETTY_PRINT) . "</pre>";
  }

} catch (PDOException $e){
  $result = array(
    "status" => "err",
    "desc" => $e->getMessage()
  );

  return $result;
}
